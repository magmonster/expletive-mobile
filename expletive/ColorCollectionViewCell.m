//
//  ColorCollectionViewCell.m
//  Expletive
//
//  Created by Drew Bombard on 4/26/18.
//  Copyright © 2018 default_method. All rights reserved.
//

#import "ColorCollectionViewCell.h"

@implementation ColorCollectionViewCell


-(UICollectionViewCell*) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{	
	ColorCollectionViewCell *cell =	[collectionView
									 dequeueReusableCellWithReuseIdentifier:@"cell"
									 forIndexPath:indexPath];
	cell.indexPath = indexPath;
	return cell;
}

@end
