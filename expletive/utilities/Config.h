//
//  Config.h
//  Expletive
//
//  Created by Drew Bombard on 5/30/18.
//  Copyright © 2018 Default Method. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PersistentData.h"


@interface Config : NSObject

+(NSNumber *) rate;
+(NSString *) clean;
+(NSString *) background_color;
+(NSString *) selected_alert_index;
+(NSDictionary *) config_array;


@end
