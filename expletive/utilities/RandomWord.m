//
//  RandomFacts.m
//  Expletive
//
//  Created by Drew Bombard on 5/30/18.
//  Copyright © 2018 Default Method. All rights reserved.
//

#import "RandomWord.h"

@implementation RandomWord

// **** Deprecated **** //
+(NSString *) word
{
	NSString *random_word = [[self all_words_array] objectAtIndex:[self randomNumber]];
	return random_word;
}




+(NSDictionary *) word_array
{
	NSDictionary *random_word_array = [[self all_words_array] objectAtIndex:[self randomNumber]];
	return random_word_array;
}
//+(NSArray *) word_array
//{
//	NSArray *random_word_array = [[self all_words_array] objectAtIndex:[self randomNumber]];
//	return random_word_array;
//}


+(int) randomNumber
{
	return [self getRandomNumberBetween:0 and:(int)[[self all_words_array] count] - 1];
}


+(int) getRandomNumberBetween:(int)from and:(int)to
{
	return (int)from + arc4random() % (to-from+1);
}


+(NSArray *) all_words_array
{
	return [[PersistentData fetchLocalData] objectForKey:@"words"];
}

@end
