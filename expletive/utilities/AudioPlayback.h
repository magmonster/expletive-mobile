//
//  AudioPlayback.h
//  Expletive
//
//  Created by Drew Bombard on 4/23/15.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>

@interface AudioPlayback : NSObject {
	
	AVAudioPlayer *audioPlayer;

}

@property (nonatomic, retain) AVAudioPlayer *audioPlayer;


+(void)prepareToPlay;
+(void)playSoundFX: (NSString *)sound_file : (NSString *)sound_type;


@end
