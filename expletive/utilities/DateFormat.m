//
//  DateFormat.m
//  CheckAge
//
//  Created by Drew Bombard on 4/6/17.
//  Copyright © 2017 default_method. All rights reserved.
//

#import "DateFormat.h"

@implementation DateFormat


#pragma mark - Formatting Utilities
+(NSDateFormatter *)dateFormatter
{
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	dateFormatter.timeStyle = NSDateFormatterNoStyle;
	[dateFormatter setDateFormat:[[PersistentData fetchLocalData] objectForKey:@"date_format"]];

	return dateFormatter;
}


+(NSString *)curentDateStringFromDate:(NSDate *)dateTimeInLine withFormat:(NSString *)dateFormat
{
	NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
	[formatter setDateFormat:dateFormat];
	NSString *convertedString = [formatter stringFromDate:dateTimeInLine];
	
	return convertedString;
}


#pragma mark - Getters
+(NSString *)getLanguage
{
	return [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
}


+(NSString *)getLocale
{
	// DEBUG
	return [[NSLocale autoupdatingCurrentLocale] objectForKey: NSLocaleCountryCode];
}


+(NSString *)getCurrentYear
{
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"yyyy"];
	return [formatter stringFromDate:[NSDate date]];
}


+(NSString *)getTodayDate
{
	NSDate *now = [NSDate date];
	return [[self dateFormatter] stringFromDate:now];
}


@end
