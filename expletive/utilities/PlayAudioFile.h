//
//  PlayAudioFile.h
//  Expletive
//
//  Created by Drew Bombard on 10/10/18.
//  Copyright © 2018 default_method. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "RandomWord.h"

@interface PlayAudioFile : NSObject


+(void)playAudioFile:(int)current_audio_file_index;


@end
