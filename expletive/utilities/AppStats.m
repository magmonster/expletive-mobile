//
//  AppStats.m
//  Frolfer
//
//  Created by Drew Bombard on 2/25/15.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import "AppStats.h"

@implementation AppStats

+(NSString *) appName
{
	return [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleNameKey];
}

+(NSString *) versionNumber
{
	return [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
}

+(NSString *) buildNumber
{
	return [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
}

+(NSString *) versionAndBuild
{
	NSString * version = [self versionNumber];
	NSString * build = [self buildNumber];
	
	NSString * versionBuild = [NSString stringWithFormat: @"%@", version];
	
	if (![version isEqualToString: build]) {
		versionBuild = [NSString stringWithFormat: @"%@.%@", versionBuild, build];
	}
	
	return versionBuild;
}

+(NSString *) versionNumberFullString
{
	return [NSString stringWithFormat: @"version: %@", [self versionNumber]];
}

+(NSString *) versionNumberWithName
{
	return [NSString stringWithFormat: @"%@: %@",[self appName], [self versionNumber]];
}

@end

