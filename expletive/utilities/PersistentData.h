//
//  PersistentData.h
//  
//
//  Created by Drew Bombard on 3/1/17.
//
//

#import <Foundation/Foundation.h>

@interface PersistentData : NSObject

+(NSMutableDictionary *)fetchLocalData;

+(NSMutableDictionary *)fetchConfigData;
+(void)setConfigData:(NSObject *)object type:(NSString *)type;

@property (strong, nonatomic) NSArray *paths;
@property (strong, nonatomic) NSString *path;
@property (strong, nonatomic) NSString *documentsDirectory;



@end
