//
//  RandomFacts.h
//  Expletive
//
//  Created by Drew Bombard on 5/30/18.
//  Copyright © 2018 Default Method. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PersistentData.h"


@interface RandomWord : NSObject


+(NSString *) word;
+(int) randomNumber;
+(NSDictionary *) word_array;
+(NSArray *) all_words_array;


@end
