//
//  FetchDataArray.h
//  Frolfer
//
//  Created by Drew Bombard on 4/29/14.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"

@interface FetchDataArray : NSObject


+(NSMutableArray *)dataFromEntity:(NSString *)entityName predicateName:(NSString *)predicateName predicateValue:(NSString *)predicateValue predicateType:(NSString *)predicateType sortName:(NSString *)sortName sortASC:(BOOL *)sortASC;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@end
