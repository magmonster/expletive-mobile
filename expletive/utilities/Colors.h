//
//  Colors.h
//  Frolfer
//
//  Created by Drew Bombard on 2/5/12.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

#import "PersistentData.h"

@interface Colors: NSObject

@property (nonatomic,readonly) UIColor *btnDisabled;


// Text Colors
@property (nonatomic,readonly) UIColor *txtDark;
@property (nonatomic,readonly) UIColor *txtLight;
@property (nonatomic,readonly) UIColor *txtWhite;

@property (nonatomic,readonly) UIColor *lightYelllow;
@property (nonatomic,readonly) UIColor *medYelllow;
@property (nonatomic,readonly) UIColor *darkYelllow;

@property (nonatomic,readonly) UIColor *lightRed;
@property (nonatomic,readonly) UIColor *medRed;
@property (nonatomic,readonly) UIColor *darkRed;


// Background color arrays
@property (nonatomic,readonly) NSDictionary *arrLime;
@property (nonatomic,readonly) NSDictionary *arrMint;
@property (nonatomic,readonly) NSDictionary *arrGrass;

@property (nonatomic,readonly) NSDictionary *arrLemon;
@property (nonatomic,readonly) NSDictionary *arrButter;
@property (nonatomic,readonly) NSDictionary *arrGold;

@property (nonatomic,readonly) NSDictionary *arrFire;
@property (nonatomic,readonly) NSDictionary *arrPumpkin;
@property (nonatomic,readonly) NSDictionary *arrOrange;

@property (nonatomic,readonly) NSDictionary *arrCrimson;
@property (nonatomic,readonly) NSDictionary *arrCardinal;
@property (nonatomic,readonly) NSDictionary *arrCarmine;

@property (nonatomic,readonly) NSDictionary *arrGrape;
@property (nonatomic,readonly) NSDictionary *arrViolet;
@property (nonatomic,readonly) NSDictionary *arrDeepPurple;

@property (nonatomic,readonly) NSDictionary *arrBlue;
@property (nonatomic,readonly) NSDictionary *arrBlue2;
@property (nonatomic,readonly) NSDictionary *arrDeepBlue;
@property (nonatomic,readonly) NSDictionary *arrDarkBlue;

@property (nonatomic,readonly) NSDictionary *arrWhite;
@property (nonatomic,readonly) NSDictionary *arrGray15;
@property (nonatomic,readonly) NSDictionary *arrGray35;
@property (nonatomic,readonly) NSDictionary *arrGray50;
@property (nonatomic,readonly) NSDictionary *arrCharcoal;
@property (nonatomic,readonly) NSDictionary *arrBlack;


// All Colors
@property (nonatomic,readonly) NSArray *all_colors_arr;


// Navigation bar & text (if separate)
@property (nonatomic,readonly) UIColor *navigationBar;
@property (nonatomic,readonly) UIColor *navigationBarText;


+(Colors*)get;
+(int)getSelectedColorIndex;
+(NSDictionary *)getSelectedColor;
+(UIColor*)colorWithHexString:(NSString*)hex;

@end
