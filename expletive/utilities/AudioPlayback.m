//
//  AudioPlayback.m
// Expletive
//
//  Created by Drew Bombard on 4/23/15.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import "AudioPlayback.h"

@implementation AudioPlayback

@synthesize audioPlayer=_audioPlayer;








// // 
// // DISABLE... routing all audio through the AVAudio player
// 
+(void)playSoundFX: (NSString *)sound_file : (NSString *)sound_type {
	//	NSLog(@"FX name: %@",sound_file);
	//	NSLog(@"FX sound_type: %@",sound_type);
	
	/*
	 * NOTE:  .caf files are crashing the app for some reason.  
	 * Ignore the incoming sound_type, and just use the .mp3
	 * variant for now.
	 */
	NSString* _filePath = [[NSBundle mainBundle] 
						   pathForResource:sound_file
						   ofType:@"mp3"];
	
	//declare a system sound
	SystemSoundID soundID;
	
	//Get a URL for the sound file
	NSURL *_fileUrl = [NSURL fileURLWithPath:_filePath isDirectory:NO];
	
	//Use audio sevices to create the sound
	AudioServicesCreateSystemSoundID((__bridge CFURLRef)_fileUrl, &soundID);
	
	//Use audio services to play the sound
	AudioServicesPlaySystemSound(soundID);
}













//+(void)playSoundFX :(NSString *)sound_file :(NSString *)sound_type {
//	
//	
//	NSLog(@"");
//	
//	AVAudioPlayer *audioPlayer;
//	
//	
//	// Get the file path to the song to play.
//	NSString *filePath = [[NSBundle mainBundle] pathForResource:@"/switch" 
//														 ofType:@"mp3"];
//	
//	// Convert the file path to a URL.
//	NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:filePath];
//	
//	//Initialize the AVAudioPlayer.
//	audioPlayer = [[AVAudioPlayer alloc] 
//						initWithContentsOfURL:fileURL error:nil];
//	
//	// Preloads the buffer and prepares the audio for playing.
//	[audioPlayer prepareToPlay];
//
//	
//	//[audioPlayer setVolume:1.0f];
//	[audioPlayer play];
//	
//		NSLog(@"");
//}






// Plays a half-second audio file and preps the AVAudioPlayer to run..
+(void)prepareToPlay {
	
	NSString *filePath = [[NSBundle mainBundle]
						  pathForResource:@"silence" 
						  ofType:@"mp3"];
	
	// Convert the file path to a URL.
	NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:filePath];
	
	//Initialize the AVAudioPlayer.
	AVAudioPlayer *audioPlayer;
	audioPlayer = [[AVAudioPlayer alloc] 
						initWithContentsOfURL:fileURL 
						error:nil];
	
	// Make sure the audio is at the start of the stream.
	audioPlayer.currentTime = 0;
	
	// Preloads the buffer and prepares the audio for playing.
	[audioPlayer prepareToPlay];
}




//+(void)playAudioArray:(NSString *)sound_file :(NSString *)sound_type :(int)num_sounds {
//	
//	NSLog(@"sound_file: %@",sound_file);
//	NSLog(@"sound_type: %@",sound_type);
//	NSLog(@"num_sounds: %i",num_sounds);
//	
//	AVAudioPlayer *audioPlayer;
//	
//	
//	
//	// Make array containing audio file names 
//	NSArray  *theSoundArray = [NSArray arrayWithObjects:@"hitface",@"hello",nil];
//	
//	int totalSoundsInQueue = (int)[theSoundArray count];
//	
//	for (int i = 0; i < totalSoundsInQueue;) {
//		
//		NSString *sound = [theSoundArray objectAtIndex:i];
//		
//		// Wait until the audio player is not playing anything
//		while(![audioPlayer isPlaying]){
//			
//			AVAudioPlayer *player = [[AVAudioPlayer alloc] 
//									 initWithContentsOfURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] 
//																				   pathForResource:sound 
//																				   ofType:@"mp3"]] error:NULL];
//			audioPlayer = player;
//			// ARC removal			[player release];
//			[audioPlayer setVolume:1.0f];
//			[audioPlayer play];
//			
//			//Increment for loop counter & move onto next iteration
//			i++;
//		}
//	} 
//	
//	
//}

@end
