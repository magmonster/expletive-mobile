//
//  Colors.m
//  Frolfer
//
//  Created by Drew Bombard on 2/5/12.
//  Copyright (c) 2013 default_method. All rights reserved.
//

#import "Colors.h"

@implementation Colors

-(id)init {
	self = [super init];
	if (self) {
		
		NSNull *_null = [NSNull null];
		
		_btnDisabled = [Colors colorWithHexString:@"F5F5F5"];
		
		// Text Color(s)
		_txtDark = [Colors colorWithHexString:@"333333"];
		_txtLight = [Colors colorWithHexString:@"FFE885"];
		_txtWhite = [Colors colorWithHexString:@"FFFFFF"];
		
		_lightYelllow = [Colors colorWithHexString:@"FFE885"];
		_medYelllow = [Colors colorWithHexString:@"FADB59"];
		_darkYelllow = [Colors colorWithHexString:@"F4CD2D"];
		
		_lightRed = [Colors colorWithHexString:@"F2838F"];
		_medRed = [Colors colorWithHexString:@"ED5565"];
		_darkRed = [Colors colorWithHexString:@"E92348"];
		
		
		/*
		 - Color Name
		 - Background color
		 - Main text color
		 - Button Color
		 - Button text color
		 */
		
		// Greens
		_arrLime 	= @{
						@"name" : @"Lime",
						@"background" : [Colors colorWithHexString:@"65CB46"],
						@"text" : _txtWhite,
						@"button_color" : _txtWhite,
						@"button_txt_color" : _txtWhite,
						@"button_border" : _null
						};
		_arrMint 	= @{
						@"name" : @"Mint",
						@"background" : [Colors colorWithHexString:@"4CAF50"],
						@"text" : _txtWhite,
						@"button_color" : _txtWhite,
						@"button_txt_color" : _txtWhite,
						@"button_border" : _null
						};
		_arrGrass 	= @{
						@"name" : @"Grass",
						@"background" : [Colors colorWithHexString:@"4E8E47"],
						@"text" : _txtWhite,
						@"button_color" : _txtWhite,
						@"button_txt_color" : _txtWhite,
						@"button_border" : _null
						};

		
		//Yellows
		_arrButter 	= @{
						@"name" : @"Butter",
						@"background" : [Colors colorWithHexString:@"FEDc56"],
						@"text" : _txtDark,
						@"button_color" : _txtDark,
						@"button_txt_color" : _txtDark,
						@"button_border" : _null
						};
		_arrLemon 	= @{
						@"name" : @"Lemon",
						@"background" : [Colors colorWithHexString:@"F5E749"],
						@"text" : _txtDark,
						@"button_color" : _txtDark,
						@"button_txt_color" : _txtDark,
						@"button_border" : _null
						};
		_arrGold 	= @{
						@"name" : @"Gold",
						@"background" : _darkYelllow,
						@"text" : _txtWhite,
						@"button_color" : _txtWhite,
						@"button_txt_color" : _txtWhite,
						@"button_border" : _null
						};
		
		
		// Oranges
		_arrFire	= @{
						@"name" : @"Fire",
						@"background" : [Colors colorWithHexString:@"FDA50F"],
						@"text" : _txtWhite,
						@"button_color" : _txtWhite,
						@"button_txt_color" : _txtWhite,
						@"button_border" : _null
						};
		_arrPumpkin = @{
						@"name" : @"Pumpkin",
						@"background" : [Colors colorWithHexString:@"FD8608"],
						@"text" : _txtWhite,
						@"button_color" : _txtWhite,
						@"button_txt_color" : _txtWhite,
						@"button_border" : _null
						};
		_arrOrange	= @{
						@"name" : @"Orange",
						@"background" : [Colors colorWithHexString:@"FD6600"],
						@"text" : _txtWhite,
						@"button_color" : _txtWhite,
						@"button_txt_color" : _txtWhite,
						@"button_border" : _null
						};
		
		
		// Reds
		_arrCrimson	= @{
						@"name" : @"Crimson",
						@"background" : [Colors colorWithHexString:@"E9193A"],
						@"text" : _txtWhite, @"button_color" : _txtWhite,
						@"button_txt_color" : _txtWhite,
						@"button_border" : _null
						};
		_arrCardinal= @{
						@"name" : @"Cardinal",
						@"background" : [Colors colorWithHexString:@"D31735"],
						@"text" : _txtWhite, @"button_color" : _txtWhite,
						@"button_txt_color" : _txtWhite,
						@"button_border" : _null
						};
		_arrCarmine = @{
						@"name" : @"Carmine",
						@"background" : [Colors colorWithHexString:@"B8122D"],
						@"text" : _txtWhite,
						@"button_color" : _txtWhite,
						@"button_txt_color" : _txtWhite,
						@"button_border" : _null
						};
		
		
		// Purple
		_arrGrape 	= @{
						@"name" : @"Grape",
						@"background" : [Colors colorWithHexString:@"B067B0"],
						@"text" : _txtWhite,
						@"button_color" : _txtWhite,
						@"button_txt_color" : _txtWhite,
						@"button_border" : _null
						};
		_arrViolet	= @{
						@"name" : @"Violet",
						@"background" : [Colors colorWithHexString:@"712171"],
						@"text" : _txtWhite, @"button_color" : _txtWhite,
						@"button_txt_color" : _txtWhite,
						@"button_border" : _null
						};
		_arrDeepPurple = @{
						@"name" : @"Deep Purple",
						@"background" : [Colors colorWithHexString:@"531A53"],
						@"text" : _txtWhite,
						@"button_color" : _txtWhite,
						@"button_txt_color" : _txtWhite,
						@"button_border" : _null
						};
	
		
		// Blues
		_arrBlue	= @{
						@"name" : @"Blue",
						@"background" : [Colors colorWithHexString:@"0A2B4B"],
						@"text" : _txtWhite,
						@"button_color" : _txtWhite,
						@"button_txt_color" : _txtWhite,
						@"button_border" : _null
						};
		_arrDeepBlue = @{
						@"name" : @"Deep Blue",
						@"background" : [Colors colorWithHexString:@"094074"],
						@"text" : _txtWhite,
						@"button_color" : _txtWhite,
						@"button_txt_color" : _txtWhite,
						@"button_border" : _null
						};
		_arrDarkBlue = @{
						@"name" : @"Dark Blue",
						@"background" : [Colors colorWithHexString:@"0A2B4B"],
						@"text" : _txtWhite,
						@"button_color" : _txtWhite,
						@"button_txt_color" : _txtWhite,
						@"button_border" : _null
						};
		
		
		// Black and Grays
		_arrWhite = @{
						@"name" : @"White",
						@"background" : [UIColor whiteColor],
						@"text" : _txtDark,
						@"button_color" : _txtDark,
						@"button_txt_color" : _txtDark,
						@"button_border" : [Colors colorWithHexString:@"0C0C0C"]
						};
		_arrGray15	= @{
						@"name" : @"Gray 15%",
						@"background" : [Colors colorWithHexString:@"D9D9D9"],
						@"text" : _txtWhite,
						@"button_color" : _txtDark,
						@"button_txt_color" : _txtWhite,
						@"button_border" : _null
						};
		_arrGray35	= @{
						@"name" : @"Gray 35%",
						@"background" : [Colors colorWithHexString:@"A0A0A0"],
						@"text" : _txtWhite,
						@"button_color" : _txtDark,
						@"button_txt_color" : _txtWhite,
						@"button_border" : _null
						};
		_arrGray50 = @{
						@"name" : @"Gray 50%",
						@"background" : [Colors colorWithHexString:@"666666"],
						@"text" : _txtWhite,
						@"button_color" : _txtWhite,
						@"button_txt_color" : _txtWhite,
						@"button_border" : _null
						};
		_arrCharcoal = @{
						@"name" : @"Charcoal",
						@"background" : [Colors colorWithHexString:@"333333"],
						@"text" : _txtWhite,
						@"button_color" : _txtWhite,
						@"button_txt_color" : _txtWhite,
						@"button_border" : _null
						};
		_arrBlack	= @{
						@"name" : @"Black",
						@"background" : [Colors colorWithHexString:@"0C0C0C"],
						@"text" : _txtWhite,
						@"button_color" : _txtWhite,
						@"button_txt_color" : _txtWhite,
						@"button_border" : _null
						};
		
		
		_all_colors_arr = @[_arrLime
							,_arrMint
							,_arrGrass
							,_arrButter
							,_arrLemon
							,_arrGold
							,_arrFire
							,_arrPumpkin
							,_arrOrange
							,_arrCrimson
							,_arrCardinal
							,_arrCarmine
							,_arrGrape
							,_arrViolet
							,_arrDeepPurple
							,_arrBlue
							,_arrDeepBlue
							,_arrDarkBlue
							,_arrWhite
							,_arrGray15
							,_arrGray35
							,_arrGray50
							,_arrCharcoal
							,_arrBlack
							];
	}
	return self;
}

+(Colors *)get
{
	static Colors* colors = nil;
	if (!colors) {
		colors = [[Colors alloc] init];
	}
	return colors;
}


#pragma mark - Selected Colors
+(int)getSelectedColorIndex
{
	return [[[PersistentData fetchConfigData] objectForKey:@"background_color"] intValue];
}


+(NSDictionary *)getSelectedColor
{
	return [[Colors get].all_colors_arr objectAtIndex: [self getSelectedColorIndex]];
}


+(UIColor *) colorWithHexString: (NSString *) hex
{
	// Performs conversion from hex string to color.
	NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];

	// String should be 6 or 8 characters  
	if ([cString length] < 6) return [UIColor grayColor];  

	// strip 0X if it appears  
	if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];  

	if ([cString length] != 6) return  [UIColor grayColor];  

	// Separate into r, g, b substrings  
	NSRange range;  
	range.location = 0;  
	range.length = 2;  
	NSString *rString = [cString substringWithRange:range];  

	range.location = 2;  
	NSString *gString = [cString substringWithRange:range];  

	range.location = 4;  
	NSString *bString = [cString substringWithRange:range];  

	// Scan values  
	unsigned int r, g, b;  
	[[NSScanner scannerWithString:rString] scanHexInt:&r];  
	[[NSScanner scannerWithString:gString] scanHexInt:&g];  
	[[NSScanner scannerWithString:bString] scanHexInt:&b];  

	return [UIColor colorWithRed:((float) r / 255.0f)  
						   green:((float) g / 255.0f)  
							blue:((float) b / 255.0f)  
						   alpha:1.0f];  
}

@end
