//
//  Config.m
//  Expletive
//
//  Created by Drew Bombard on 5/30/18.
//  Copyright © 2018 Default Method. All rights reserved.
//

#import "Config.h"

@implementation Config


+(NSDictionary *) config_array
{
	return [PersistentData fetchConfigData];
}

+(NSNumber *) rate
{
	return [[self config_array] objectForKey:@"rate"];
}

+(NSString *) clean
{
	return [ [self config_array] objectForKey:@"clean"];
}

+(NSString *) background_color
{
	return [ [self config_array] objectForKey:@"background_color"];
}

+(NSString *) selected_alert_index
{
	return [ [self config_array] objectForKey:@"selected_alert_index"];
}

@end
