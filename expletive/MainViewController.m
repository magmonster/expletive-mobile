//
//  MainViewController.m
// Expletive
//
//  Created by Drew Bombard on 4/30/15.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>


// Data
#import "Config.h"
#import "RandomWord.h"


// Utilities
#import "Colors.h"
#import "AppStats.h"
#import "AudioPlayback.h"


@class MainViewController;


@interface MainViewController : UIViewController <AVAudioPlayerDelegate>
{
	int fileArrCount;
	int currentValue;
	int currentCount;
	int audioFileIndex;
	AVAudioPlayer *audioPlayer;
}


// Misc.
@property (nonatomic) int max_swear_count;
@property (strong, nonatomic) IBOutlet UIView *view_background;


// On screen feedback
@property (nonatomic) int totalSwearCount;
@property (strong, nonatomic) IBOutlet UILabel *lbl_num_curses;
@property (strong, nonatomic) IBOutlet UILabel *lbl_current_curse;
@property (strong, nonatomic) IBOutlet UILabel *lbl_active_cuss_word;

// Custom views (overlay & progress indicator)
@property (weak, nonatomic) IBOutlet UIView *overlay;
@property (strong, nonatomic) IBOutlet UIProgressView *progressMeter;


// Buttons
@property (strong, nonatomic) IBOutlet UIButton *btn_clear;
@property (strong, nonatomic) IBOutlet UIButton *btn_stop;
@property (strong, nonatomic) IBOutlet UIButton *btn_start;
@property (strong, nonatomic) IBOutlet UIButton *btn_decrease;
@property (strong, nonatomic) IBOutlet UIButton *btn_increase;
@property (strong, nonatomic) IBOutlet UIButton *btn_settings;


// Actions / Gestures
@property (strong, nonatomic) NSTimer *longPressTimer;
@property (strong, nonatomic) UILongPressGestureRecognizer *lpgr;


// Audio Playback
-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag;
-(void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error;


// Local Data
@property (strong, nonatomic) NSDictionary *config_arr;
@property (strong, nonatomic) NSArray *all_words_array;
@property (strong, nonatomic) NSString *random_word_num;
@property (strong, nonatomic) NSDictionary *random_word_array;


@end



@implementation MainViewController


#pragma mark - App Setup
-(void)viewDidLoad
{
	[super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
	
	[self resetCounts];
	[self customizeInterface];
	[self clearSwearCount:nil];
	

	// Data
	_config_arr = [Config config_array];
	_all_words_array = [RandomWord all_words_array];
	_max_swear_count = (int)[[RandomWord all_words_array] count];
	
	
	// Prep for playback
	audioFileIndex = 0;
	[AudioPlayback prepareToPlay];

	
	// Init the Long Press Gesture (on the '+' button)
	self.lpgr = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressGestures:)];
	self.lpgr.allowableMovement = 100.0f;
	self.lpgr.minimumPressDuration = 0.5f;
	[_btn_increase addGestureRecognizer:self.lpgr];
}


-(void)viewWillAppear:(BOOL)animated
{
	// Data
	_config_arr = [Config config_array];
	
	[self setupColorTheme];
}


-(void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
}


- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

#pragma mark - U.I. theme
-(void)setupColorTheme
{
	/*
	 * Change the label of the Start button
	 * depending on if the "censor" option is chosen
	 */
	if ([[_config_arr objectForKey:@"censor"] intValue] == 1) {
		[_btn_start setTitle:@"Start" forState:UIControlStateNormal];
	} else {
		[_btn_start setTitle:@"!*#%" forState:UIControlStateNormal];
	}
	
	_btn_stop.backgroundColor = [Colors get].darkRed;
	_btn_stop.titleLabel.textColor = [UIColor whiteColor];
	[_btn_stop setTitleColor: [UIColor whiteColor] forState:UIControlStateNormal];
	
	
	/*
	 * Add/Subtract buttons need their rendering mode altered if they are using
	 * the white theme, so that we can tint it to a darker color. Otherwise, strip
	 * rendering mode and retain the white appearance for other colors.
	 */
	UIImage *img_add = [[UIImage imageNamed:@"btn_add"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
	UIImage *img_minus = [[UIImage imageNamed:@"btn_minus"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
	UIImage *img_settings = [[UIImage imageNamed:@"settings"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
	
	[_btn_increase setImage:img_add forState:UIControlStateNormal];
	[_btn_decrease setImage:img_minus forState:UIControlStateNormal];
	[_btn_settings setImage:img_settings forState:UIControlStateNormal];
	
	if ([[[Colors getSelectedColor] objectForKey:@"name"]  isEqual: @"White"]) {
		_btn_increase.tintColor = [[Colors getSelectedColor] objectForKey:@"button_border"];
		_btn_decrease.tintColor = [[Colors getSelectedColor] objectForKey:@"button_border"];
	} else {
		_btn_increase.tintColor = [UIColor clearColor];
		_btn_decrease.tintColor = [UIColor clearColor];
	}
	
	_btn_settings.tintColor = [[Colors getSelectedColor] objectForKey:@"button_color"];
	
	
	// Text/Label Elements
	_lbl_num_curses.textColor = [[Colors getSelectedColor] objectForKey:@"text"];
	_lbl_current_curse.textColor = [[Colors getSelectedColor] objectForKey:@"text"];
	_lbl_active_cuss_word.textColor = [[Colors getSelectedColor] objectForKey:@"text"];
	
	
	// Start button
	_btn_start.backgroundColor = [[Colors getSelectedColor] objectForKey:@"button_color"];
	_btn_start.titleLabel.textColor = [[Colors getSelectedColor] objectForKey:@"background"];
	[_btn_start setTitleColor:  [[Colors getSelectedColor] objectForKey:@"background"] forState:UIControlStateNormal];
	
	
	// Primary background color
	_view_background.backgroundColor = [[Colors getSelectedColor] objectForKey:@"background"];
}


#pragma mark - Button Actions
-(IBAction)btnIncreaseCount:(id)sender
{
	[self update_swear_count:@"increase"];
	[self checkButtonStatus];
}


- (IBAction)btnDecreaseCount:(id)sender
{
	if (_totalSwearCount == 0) {
		[self clearSwearCount:nil];
	}
	[self update_swear_count:@"decrease"];
	[self checkButtonStatus];
}


-(IBAction)clearSwearCount:(id)sender
{
	_btn_clear.hidden = YES;
	
	[self resetCounts];
	[self disableStart];
	[self checkButtonStatus];
}


-(IBAction)stop_ranting:(id)sender
{
	[AudioPlayback playSoundFX:@"/switch" :@"mp3"];
	if (audioPlayer.playing == YES) {
		[audioPlayer stop];
		[self resetButtons:0];
	}
}


-(IBAction)start_ranting:(id)sender
{
	if (audioPlayer.playing == YES) {
		
		[self hideCurrentCurse];
		
		[audioPlayer stop];
		[self resetButtons:0];
	} else {
		
		[self showCurrentCurse];
		
		[AudioPlayback playSoundFX:@"/switch" :@"mp3"];
		[self resetButtons:1];
		[self playAudioFile: audioFileIndex];
	}
}


-(void)hideCurrentCurse
{
	CATransition *animation = [CATransition animation];
	animation.type = kCATransitionFade;
	animation.duration = 0.4;
	[_lbl_active_cuss_word.layer addAnimation:animation forKey:nil];
	
	_lbl_active_cuss_word.hidden = YES;
}


-(void)showCurrentCurse
{
	CATransition *animation = [CATransition animation];
	animation.type = kCATransitionFade;
	animation.duration = 0.4;
	[_lbl_active_cuss_word.layer addAnimation:animation forKey:nil];
	
	_lbl_active_cuss_word.hidden = NO;
}


-(void)hideClear
{
	CATransition *animation = [CATransition animation];
	animation.type = kCATransitionFade;
	animation.duration = 0.4;
	[_lbl_active_cuss_word.layer addAnimation:animation forKey:nil];
	
	_btn_clear.hidden = YES;
}


-(void)resetCounts
{
	audioFileIndex = 0;
	_totalSwearCount = 0;
	currentCount = 0;
	
	_lbl_num_curses.text = @"0";
	_progressMeter.progress = 0.00f;
}


#pragma mark - Audio Setup
-(void)longPressCounterIncrement
{
	NSLog(@"\n");
	NSLog(@"longPressCounterIncrement");
	
	SystemSoundID soundID;
	NSString *soundPath;
	NSURL *soundURL;
	soundPath = [[NSBundle mainBundle] pathForResource:@"/clickFast" ofType:@"mp3"];
	soundURL = [NSURL fileURLWithPath:soundPath];
	[self playButtonClickNormal:&soundID soundURL:soundURL];
	
	
	_totalSwearCount++;
	
	[self checkStartStatus];

	currentValue = _totalSwearCount;
	fileArrCount = _totalSwearCount - 1;
	
	_lbl_num_curses.text = [NSString stringWithFormat:@"%d", currentValue];
}


#pragma mark - Audio Playback / Sound FX
-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
	if (audioFileIndex <= fileArrCount) {
		[self playAudioFile:audioFileIndex];
	} else {
		
		[self hideClear];
		[self resetCounts];
		[self disableStart];
		[self resetButtons:0];
		[self hideCurrentCurse];
		[self checkButtonStatus];
	}
}


-(void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error
{
	NSLog(@"audioPlayerDecodeErrorDidOccur");
}


-(void)playAudioFile:(int)current_audio_file_index
{
	audioFileIndex++;
	NSString *audio_file;
	NSString *current_word;
	
	_random_word_array = [RandomWord word_array];
	_random_word_num = [NSString localizedStringWithFormat:@"%d", [RandomWord randomNumber]];
	
	
	if (current_audio_file_index <= fileArrCount) {
		
		currentCount++;
		
		
		// Play the "censor beep" if they've chosen to censor their audio
		if ([[_config_arr objectForKey:@"censor"] intValue] == 1 &&  [[_random_word_array objectForKey:@"clean"] intValue] == 0 ) {
			audio_file = @"censor";
			current_word = @"* * * *";
		} else {
			audio_file = [[_random_word_array objectForKey:@"file"] stringByReplacingOccurrencesOfString:@".mp3" withString:@""];
			current_word = [_random_word_array objectForKey:@"name"];
		}
		
		NSString *path = [[NSBundle mainBundle] pathForResource:audio_file ofType:@"mp3"];
		audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];
		audioPlayer.delegate = self;
		audioPlayer.volume = 1.0;
		audioPlayer.numberOfLoops = 0;
		audioPlayer.enableRate = YES;
		
		audioPlayer.rate = [[_config_arr objectForKey:@"rate"] floatValue];
		
		[audioPlayer setMeteringEnabled: YES];
		[audioPlayer play];
		
		
		_lbl_active_cuss_word.text = current_word;
		_lbl_num_curses.text = [NSString stringWithFormat:@"%d", currentValue];
		
		currentValue--;
		
		NSLog(@"_totalSwearCount: %i",_totalSwearCount);
		NSLog(@"currentValue %i",currentValue);
		
		NSLog(@"float %f", (float)currentCount / _totalSwearCount);
		
		[self.progressMeter setProgress: (float)currentCount/_totalSwearCount animated:YES];
	}
}


-(void)playButtonClickNormal:(SystemSoundID *)soundID soundURL:(NSURL *)soundURL
{
	AudioServicesCreateSystemSoundID ((__bridge CFURLRef)soundURL, soundID);
	AudioServicesPlaySystemSound(*soundID);
}


#pragma mark - Start / Stop / LongPress
-(void)handleLongPressGestures:(UILongPressGestureRecognizer *)sender
{
	if ([sender isEqual:self.lpgr]) {
		if (sender.state == UIGestureRecognizerStateBegan) {
			
			_longPressTimer = [NSTimer scheduledTimerWithTimeInterval:0.07 target:self selector:@selector(longPressCounterIncrement) userInfo:nil repeats:YES];
			
			NSLog(@"start");
		}
		
		if (sender.state == UIGestureRecognizerStateEnded) {
			
			[_longPressTimer invalidate];
			_longPressTimer = nil;
			
			
			currentValue = _totalSwearCount;
			
			NSLog(@"counter: %d",_totalSwearCount);
			NSLog(@"currentValue: %d",currentValue);

			NSLog(@"STOP");
		}
	}
}


-(void)resetButtons:(int)isRanting
{
	switch (isRanting) {
		case 0: // Stop rant and audio playback
			audioFileIndex = 0;

			_overlay.hidden = YES;

			[self disableStop];
			break;
		
		case 1: // Start/continue rant and audio playback.

			_overlay.hidden = NO;

			[self disableStart];
			[self enableStop];
			break;
			
		default:
			break;
	}
}


-(void)disableStart
{
	_btn_start.alpha = 0.2;
	_btn_start.enabled = NO;
	_btn_clear.hidden = YES;
}


-(void)enableStart
{
	_btn_clear.hidden = NO;
	_btn_start.alpha = 1.0;
	_btn_start.enabled = YES;
}


-(void)disableStop
{
	[self checkButtonStatus];
	
	_btn_stop.hidden = YES;
	_btn_stop.enabled = NO;
}


-(void)enableStop
{
	_btn_stop.hidden = NO;
	_btn_stop.enabled = YES;
}


#pragma mark - Swear Counter
-(void)update_swear_count:(NSString *)count_modifier
{
	SystemSoundID soundID;
	NSString *soundPath;
	NSURL *soundURL;
	
	_random_word_array = [RandomWord word_array];
	_random_word_num = [NSString localizedStringWithFormat:@"%d", [RandomWord randomNumber]];
	
	NSLog(@"_random_word_array: %@",_random_word_array);

	
	
	if ([count_modifier isEqualToString: @"increase"]) {
		
		soundPath = [[NSBundle mainBundle] pathForResource:@"/click_plus" ofType:@"mp3"];
		
		
		/* max_swear_cout: Only allows the counter to increase 
		 * if there are enough sounds in the local database.
		 */
		if (_totalSwearCount < _max_swear_count) {
			_totalSwearCount++;
			[_btn_start setEnabled:TRUE];
		}
	} else {
		
		soundPath = [[NSBundle mainBundle] pathForResource:@"click_minus" ofType:@"mp3"];
		
		if (_totalSwearCount != 0) {
			_totalSwearCount--;
		}
	}
	
	
	// Play button click sound
	soundURL = [NSURL fileURLWithPath:soundPath];
	[self playButtonClickNormal:&soundID soundURL:soundURL];
	

	// Reset button colors / status's
	[self checkStartStatus];
	
	
	currentValue = _totalSwearCount;
	

	_lbl_num_curses.text = [NSString stringWithFormat:@"%d", _totalSwearCount];
	
	// Knock it back by one so that we start at the zero point...
	fileArrCount = _totalSwearCount - 1;
	
	
	//Debug Data
	NSLog(@"count_modifier: %@",count_modifier);
	NSLog(@"_totalSwearCount: %d",_totalSwearCount);
	NSLog(@"_max_swear_count: %i",_max_swear_count);
	NSLog(@"\n");
}


-(void)checkStartStatus
{
	if (_totalSwearCount == 0) {
		[self resetCounts];
		[self disableStart];
		[self hideCurrentCurse];
	} else {
		[self enableStart];
		[self checkButtonStatus];
	}
}


-(void)customizeInterface
{
	// Shadow and rounded edges
	[self configShadowLayer:_btn_stop
			   cornerRadius:15.0
			  shadowOffsetX:2.0
			  shadowOffsetY:2.0
			   shadowRadius:4.0
					opacity:0.3];
	
	[self configShadowLayer:_btn_start
			   cornerRadius:15.0
			  shadowOffsetX:2.0
			  shadowOffsetY:2.0
			   shadowRadius:4.0
					opacity:0.3];
	
	[self disableStart];
	[self disableStop];
	
	self.lpgr = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressGestures:)];
	self.lpgr.minimumPressDuration = 0.5f;
	self.lpgr.allowableMovement = 100.0f;
	
	[_btn_increase addGestureRecognizer:self.lpgr];
}


-(void)configShadowLayer:(UIView *)shadowView
			cornerRadius:(float)cornerRadius
		   shadowOffsetX:(float)shadowOffsetX
		   shadowOffsetY:(float)shadowOffsetY
			shadowRadius:(float)shadowRadius
				 opacity:(float)opacity
{
	CALayer *shadowLayer = shadowView.layer;
	shadowLayer.masksToBounds = NO;
	shadowLayer.cornerRadius = cornerRadius;
	shadowLayer.shadowOffset = CGSizeMake(shadowOffsetX, shadowOffsetY);
	shadowLayer.shadowRadius = shadowRadius;
	shadowLayer.shadowOpacity = opacity;
}


-(void)checkButtonStatus
{
	if (_totalSwearCount >= 1) {
		
		[self enableStart];
		
		_btn_decrease.alpha = 0.6;
		_btn_decrease.enabled = YES;
		
	} else {
		
		[self disableStart];
		
		_btn_decrease.alpha = 0.2;
		_btn_decrease.enabled = NO;
	}
}

@end
