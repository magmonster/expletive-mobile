//
// AppDelegate.m
// Expletive
//
// Created by Drew Bombard on 5/16/15.
// Copyright (c) 2015 default_method. All rights reserved.
//

#import "AppDelegate.h"
#import "MainViewController.h"

#if TARGET_OS_IOS
	@import Firebase;
#endif





@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
	// Override point for customization after application launch.


	#if TARGET_OS_IOS

	[FIRApp configure];
	[self customizeInterface];
	
	#endif

	return YES;
}

- (void)customizeInterface {
	
	[self defineHeightWidth];
	[self defineDeviceSize];
	
	#ifdef LITE
		_isFreeVersion = YES;
	#else
		_isFreeVersion = NO;
	#endif

}


-(void)forceCrash {
	//	[[Crashlytics sharedInstance] crash];
}

-(void)defineHeightWidth {
	CGRect screenRect = [[UIScreen mainScreen] bounds];
	_screenWidth = screenRect.size.width;
	_screenHeight = screenRect.size.height;
}

-(void)defineDeviceSize {
	
	UIDevice *device = [UIDevice currentDevice];
	//UIDeviceOrientation currentOrientation = device.orientation;
	BOOL isPhone = (device.userInterfaceIdiom == UIUserInterfaceIdiomPhone);
	
	NSLog(@"mainScreen height: %f", [[UIScreen mainScreen] bounds].size.height);
	NSLog(@"mainScreen width: %f", [[UIScreen mainScreen] bounds].size.width);
	
	int heightComparison = [[UIScreen mainScreen] bounds].size.height;
	NSLog(@"heightComparison: %d", heightComparison);
	
	// Do Portrait Things
	if (isPhone == YES) {
		
		// Do Portrait Phone Things
		switch (heightComparison) {
			case 480:
				NSLog(@"Legacy Phone");
				_deviceHeight = 480;
				_isPhoneLegacy = YES;
				break;
			case 568:
				NSLog(@"iPhone 5");
				_deviceHeight = 568;
				_isPhone5 = YES;
				break;
			case 667:
				NSLog(@"iPhone 6");
				_deviceHeight = 667;
				_isPhone6 = YES;
				break;
			case 736:
				NSLog(@"iPhone  6 Plus");
				_deviceHeight = 736;
				_isPhone6Plus = YES;
				break;
			default:
				break;
		}
		
	} else {
		// Do Portrait iPad things.
	}
}





- (void)applicationWillResignActive:(UIApplication *)application {
	// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	// Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
	// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
	// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
	// Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
	// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
	// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
	// Saves changes in the application's managed object context before the application terminates.
}


@end
