//
//  AppDelegate.h
// Expletive
//
//  Created by Drew Bombard on 5/16/15.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@property (assign) BOOL isFreeVersion;

@property (assign) BOOL isPhone5;
@property (assign) BOOL isPhone6;
@property (assign) BOOL isPhone6Plus;
@property (assign) BOOL isPhoneLegacy;

@property (assign) CGFloat deviceHeight;
@property (assign) CGFloat screenHeight;
@property (assign) CGFloat screenWidth;


@end

