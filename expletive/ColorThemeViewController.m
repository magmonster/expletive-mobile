//
//  ColorThemeViewController.m
//  Expletive
//
//  Created by Drew Bombard on 4/6/18.
//  Copyright © 2018 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>

// Utilities
#import "Colors.h"
#import "ColorCollectionViewCell.h"

// Data
#import "PersistentData.h"

@interface ColorThemeViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource>

@property (strong, nonatomic) NSArray *all_colors_arr;
@property (readonly, nonatomic) int selected_color_index;
@property (strong, nonatomic) IBOutlet UICollectionView *color_collection_view;

-(IBAction)changeTheme:(UIControl *)sender;

@end

@implementation ColorThemeViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	
	_selected_color_index = [[[PersistentData fetchConfigData] objectForKey:@"background_color"] intValue];
	_all_colors_arr = [Colors get].all_colors_arr;
	[_color_collection_view reloadData];
}


-(void)viewWillAppear:(BOOL)animated
{
	[self.navigationController.navigationBar setTitleTextAttributes:
	 @{NSForegroundColorAttributeName:[[Colors getSelectedColor] objectForKey:@"text"]}];
	self.navigationController.navigationBar.tintColor = [[Colors getSelectedColor] objectForKey:@"text"];
}


- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}


#pragma mark - CollectionView Config
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
	return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
	return [_all_colors_arr count];
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
	/*
	 * 0 - NSString: color name
	 * 1 - UIColor: color value
	 * 2 - UIColor: text color
	 * 3 - UIStatusBarStyle
	 * 4 - UIColor: border color (if applicable)
	 */
	static NSString *cellIdentifier = @"cell";
	ColorCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
	
	CGColorRef borderColor;
	
	if ([_all_colors_arr[indexPath.row] objectForKey:@"button_border"] != [NSNull null]) {
		borderColor = [[_all_colors_arr[indexPath.row] objectForKey:@"button_border"] CGColor];
	} else {
		borderColor = [[UIColor clearColor] CGColor];
	}
	
	if (indexPath.row == _selected_color_index) {
		cell.img_selected_color.image = [UIImage imageNamed:@"check_blue"];
	} else {
		cell.img_selected_color.image = nil;
	}
	
	cell.btn_color_theme.layer.cornerRadius = cell.btn_color_theme.layer.frame.size.width/2;
	cell.btn_color_theme.clipsToBounds = YES;
	
	cell.btn_color_theme.tag = indexPath.row;
	[cell.btn_color_theme.layer setBorderWidth: 1.0f];
	[cell.btn_color_theme.layer setBorderColor: borderColor];
	[cell.btn_color_theme setTitleColor:[_all_colors_arr[indexPath.row] objectForKey:@"button_txt_color"] forState:UIControlStateNormal];
	[cell.btn_color_theme setBackgroundColor:[_all_colors_arr[indexPath.row] objectForKey:@"background"]];
	[cell.btn_color_theme setTitle:[_all_colors_arr[indexPath.row] objectForKey:@"name"] forState:UIControlStateNormal];
	
	return cell;
}


#pragma mark - Actions
-(IBAction)changeTheme:(UIControl *)sender
{
	//NSLog(@"changeTheme");
	//NSLog(@"sender.tag: %ld", (long)sender.tag);
	[PersistentData setConfigData:[NSString stringWithFormat:@"%ld", (long)sender.tag] type:@"background_color"];
	[self.navigationController popViewControllerAnimated:YES];
}

@end
