//
//  SettingsTVController.h
//  ExpletiveTV
//
//  Created by Drew Bombard on 10/3/18.
//  Copyright © 2018 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>


// Data
#import "Config.h"
#import "RandomWord.h"


// Utilities
#import "Colors.h"
#import "AppStats.h"
#import "DateFormat.h"
#import "AudioPlayback.h"


@interface SettingsTVController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *lbl_style;
@property (strong, nonatomic) IBOutlet UILabel *lbl_copyright;
@property (strong, nonatomic) IBOutlet UILabel *lbl_version_num;


// Controls
@property (strong, nonatomic) IBOutlet UILabel *lbl_current_rate;
@property (strong, nonatomic) IBOutlet UIButton *btn_decreaseRate;
@property (strong, nonatomic) IBOutlet UIButton *btn_increaseRate;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segment_censor;


// Local Data
@property (strong, nonatomic) NSDictionary *config_arr;


@end
