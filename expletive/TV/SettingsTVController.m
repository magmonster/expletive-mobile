//
//  SettingsTVController.m
//  ExpletiveTV
//
//  Created by Drew Bombard on 10/3/18.
//  Copyright © 2018 default_method. All rights reserved.
//

#import "SettingsTVController.h"

@interface SettingsTVController ()

@end

@implementation SettingsTVController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	
	
	// Set the app version beneath name
	_lbl_version_num.text = [AppStats versionNumberWithName];
	_lbl_copyright.text = [NSString stringWithFormat: @"© %@ MagMonster Apps", [DateFormat getCurrentYear]];
}


-(void)viewWillAppear:(BOOL)animated
{
	[self getConfigData];
	[self setupSettingsOptions];
}


-(void)getConfigData
{
	_config_arr = [Config config_array];
}


#pragma mark - Settings Options
-(void)setupSettingsOptions
{
	NSLog(@"censor: %@", [_config_arr objectForKey:@"censor"]);
	NSLog(@"config: %@", _config_arr);
	
	if ([[_config_arr objectForKey:@"censor"] intValue] == 1) {
		_lbl_style.text = @"Clean";
		_segment_censor.selectedSegmentIndex = 0;
	} else {
		_lbl_style.text = @"!*#%";
		_segment_censor.selectedSegmentIndex = 1;
	}
}


-(IBAction)increaseRate:(id)sender
{
	double curse_rate = 0;
	
	// 3x is the max rate
	if ([[_config_arr objectForKey:@"rate"] doubleValue] < 3.00) {
		
		curse_rate = [[_config_arr objectForKey:@"rate"] doubleValue] + 0.05;
		
		[PersistentData setConfigData: [NSNumber numberWithDouble:curse_rate] type:@"rate"];
		
		// Grab config data again in case it was reset.
		[self getConfigData];
	}
	
	_lbl_current_rate.text = [[NSString stringWithFormat: @"%.2f", [[_config_arr objectForKey:@"rate"] doubleValue]] stringByAppendingString:@"x"];
}



-(IBAction)decreaseRate:(id)sender
{
	double curse_rate = 0.0;
	if ([[_config_arr objectForKey:@"rate"] doubleValue] >= 0.10f) {

		curse_rate = [[_config_arr objectForKey:@"rate"] doubleValue]- 0.05;
		
		[PersistentData setConfigData: [NSNumber numberWithDouble:curse_rate] type:@"rate"];
		
		// Grab config data again in case it was reset.
		[self getConfigData];
	}

	_lbl_current_rate.text = [[NSString stringWithFormat: @"%.2f", [[_config_arr objectForKey:@"rate"] doubleValue]] stringByAppendingString:@"x"];
}


-(IBAction)changeStyle:(UISegmentedControl *)sender
{
	NSLog(@"\n\nsegment: %ld", (long)_segment_censor.selectedSegmentIndex);

	NSLog(@"censor before: %d",[[_config_arr objectForKey:@"censor"] intValue]);
	
	// Default to 'no'
	int censor = 0;
	
	
	switch (_segment_censor.selectedSegmentIndex) {
			
		case 0:
			NSLog(@"Clean is ON. Remove the dirt.");
			censor = 1;
			break;

		default:
		case 1:
			NSLog(@"Clean is OFF. Get dirty.");
			censor = 0;
			break;
	}
	
	
	[PersistentData setConfigData: [NSString stringWithFormat:@"%d", censor] type:@"censor"];


	// Re-grab config data again in case it was reset.
	[self getConfigData];
	
	NSLog(@"censor after: %d",[[_config_arr objectForKey:@"censor"] intValue]);
	
	[self setupSettingsOptions];
}

@end
