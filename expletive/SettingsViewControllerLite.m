//
//  SettingsViewControllerLite.m
// Expletive
//
//  Created by Drew Bombard on 11/13/15.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"


// Banners
@class GADBannerView;
@import GoogleMobileAds;


@interface SettingsViewControllerLite : UIViewController <GADBannerViewDelegate>

@property (assign, nonatomic) BOOL bannerIsVisible;
@property (strong, nonatomic) AppDelegate *appDelegate;
@property (strong, nonatomic) IBOutlet UIView *containerView;
@property (strong, nonatomic) IBOutlet GADBannerView *bannerView;

@end

@implementation SettingsViewControllerLite


#pragma mark - App Setup
- (void)viewDidLoad
{
    [super viewDidLoad];
	
	_appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
	
	[[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];

	// Show the Ad Banners if this is the Free/Lite version
	if (_appDelegate.isFreeVersion == YES) {
		[self bannerConfig];
	}
}


#pragma mark - Button Actions
- (IBAction)dismissModal:(id)sender
{
	[self dismissViewControllerAnimated:YES completion:nil];
}




#pragma mark - Banner Ads
-(void)bannerConfig {
	
	NSLog(@"Google Mobile Ads SDK version: %@", [GADRequest sdkVersion]);
	
	self.bannerView.delegate = self;
	
	self.bannerView.adUnitID = @"ca-app-pub-7096232266642103/4120706876";
	self.bannerView.rootViewController = self;
	[self.bannerView loadRequest:[GADRequest request]];
}


/// Tells the delegate an ad request loaded an ad.
- (void)adViewDidReceiveAd:(GADBannerView *)adView {
	
	NSLog(@"Settings: adViewDidReceiveAd()");
	
	if (!_bannerIsVisible || _bannerIsVisible == NO) {
		
		adView.alpha = 0;
		[UIView animateWithDuration:1.0 animations:^{
			adView.alpha = 1;
		}];
		
		
	
		[UIView beginAnimations:@"animateAdBannerOn" context:NULL];
		
		// Assumes the banner view is just off the bottom of the screen.
		_bannerView.frame = CGRectOffset(_bannerView.frame, 0, -_bannerView.frame.size.height);
		
		[UIView commitAnimations];
		
		_bannerIsVisible = YES;
		
		
		// Move the container view up...
		[UIView animateWithDuration:0.2
						 animations:^{
							 CGRect frame = self.containerView.frame;
							 frame.size.height = self.view.bounds.size.height - 50;
							 self.containerView.frame = frame;
						 }];
		
	}
	
}

/// Tells the delegate an ad request failed.
- (void)adView:(GADBannerView *)adView didFailToReceiveAdWithError:(GADRequestError *)error {
	
	NSLog(@"Settings: adViewDidFailToReceiveAdWithError: %@", [error localizedDescription]);
	NSLog(@"Failed to retrieve ad");
	
	if (_bannerIsVisible || _bannerIsVisible == YES) {
		
		adView.alpha = 1;
		[UIView animateWithDuration:1.0 animations:^{
			adView.alpha = 0;
		}];
		
		
		[UIView beginAnimations:@"animateAdBannerOff" context:NULL];
		
		// Assumes the banner view is placed at the bottom of the screen.
		_bannerView.frame = CGRectOffset(_bannerView.frame, 0, _bannerView.frame.size.height);
		
		[UIView commitAnimations];
		
		_bannerIsVisible = NO;
		
		// Move the container view up...
		[UIView animateWithDuration:0.2
						 animations:^{
							 CGRect frame = self.containerView.frame;
							 frame.size.height = self.view.bounds.size.height + 50;
							 self.containerView.frame = frame;
						 }];
	}
	
}

/// Tells the delegate that a full screen view will be presented in response
/// to the user clicking on an ad.
- (void)adViewWillPresentScreen:(GADBannerView *)adView {
	NSLog(@"adViewWillPresentScreen");
}

/// Tells the delegate that the full screen view will be dismissed.
- (void)adViewWillDismissScreen:(GADBannerView *)adView {
	NSLog(@"adViewWillDismissScreen");
}

/// Tells the delegate that the full screen view has been dismissed.
- (void)adViewDidDismissScreen:(GADBannerView *)adView {
	NSLog(@"adViewDidDismissScreen");
}

/// Tells the delegate that a user click will open another app (such as
/// the App Store), backgrounding the current app.
- (void)adViewWillLeaveApplication:(GADBannerView *)adView {
	NSLog(@"adViewDidLeaveApplication");
}

@end
