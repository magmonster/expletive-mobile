//
//  SettingsTableViewController.m
// Expletive
//
//  Created by Drew Bombard on 9/8/15.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

// Data
#import "Config.h"


// Utilities
#import "Colors.h"
#import "AppStats.h"
#import "DateFormat.h"
#import "AppDelegate.h"


@interface SettingsTableViewController : UITableViewController <MFMailComposeViewControllerDelegate>

// Outlets

@property (strong, nonatomic) IBOutlet UILabel *lbl_copyright;
@property (strong, nonatomic) IBOutlet UILabel *lbl_version_num;
@property (strong, nonatomic) IBOutlet UILabel *lbl_slider_rate;
@property (strong, nonatomic) IBOutlet UILabel *lbl_color_theme;

// Controls
@property (strong, nonatomic) IBOutlet UISlider *slider;
@property (strong, nonatomic) IBOutlet UIImageView *img_logo;
@property (strong, nonatomic) IBOutlet UISwitch *switch_censor;

// Local Data
@property (strong, nonatomic) NSArray *to_recipients;
@property (strong, nonatomic) NSDictionary *config_arr;

@end



@implementation SettingsTableViewController


#pragma mark - App Setup
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
	

	// Email "From" address..
	_to_recipients = [NSArray arrayWithObjects:@"support@defaultmethod.com", nil];
	
	// Set the app version beneath name
	_lbl_version_num.text = [AppStats versionNumberWithName];
	_lbl_copyright.text = [NSString stringWithFormat: @"© %@ MagMonster Apps", [DateFormat getCurrentYear]];
}


-(void)viewWillAppear:(BOOL)animated
{
	[self getConfigData];
	
	[self setupSlider];
	[self setupSettingsOptions];
}


-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Data Setup
-(void)getConfigData
{
	_config_arr = [Config config_array];
}


#pragma mark - Settings Options
-(void)setupSettingsOptions
{
	_lbl_color_theme.text = [[Colors getSelectedColor] objectForKey:@"name"];
	
	// Logo
	_img_logo.tintColor = [[Colors getSelectedColor] objectForKey:@"background"];
	_img_logo.image = [_img_logo.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];

	
	// NavBar tinting
	self.navigationController.navigationBar.tintColor 	= [[Colors getSelectedColor] objectForKey:@"text"];
	self.navigationController.navigationBar.barTintColor = [[Colors getSelectedColor] objectForKey:@"background"];
	self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [[Colors getSelectedColor] objectForKey:@"text"]};
	

	// Set censor state
	if ([[_config_arr objectForKey:@"censor"] intValue] == 1) {
		[_switch_censor setOn:YES animated:YES];
	} else {
		[_switch_censor setOn:NO animated:NO];
	}
	_switch_censor.onTintColor = [[Colors getSelectedColor] objectForKey:@"background"];
}


#pragma mark - Slider
-(void)setupSlider
{
	_slider.minimumValue = 0.50f;
	_slider.maximumValue = 3.00f;
	_slider.value = [[_config_arr objectForKey:@"rate"] floatValue];
	
	[self updateSlider];
}

-(void)updateSlider
{
	if ([[_config_arr objectForKey:@"rate"] floatValue] != 1.0f) {
		_slider.value = [[_config_arr objectForKey:@"rate"] floatValue];
	} else {
		_slider.value = 1.0f;
	}
	
	_lbl_slider_rate.text = [[NSString stringWithFormat: @"%.2f",[[_config_arr objectForKey:@"rate"] doubleValue]] stringByAppendingFormat:@"x"];
}



#pragma mark - Mail composer

-(void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
	switch (result)
	{
		case MFMailComposeResultCancelled:
			NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
			break;
		case MFMailComposeResultSaved:
			NSLog(@"Mail saved: you saved the email message in the drafts folder.");
			break;
		case MFMailComposeResultSent:
			NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
			break;
		case MFMailComposeResultFailed:
			NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
			break;
		default:
			NSLog(@"Mail not sent.");
			break;
	}
 
	// Remove the mail view
	[self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Button Actions

- (IBAction)swearSwitch:(id)sender
{
	int censor = 0;
	
	if ([sender isOn]) {
		censor = 1;
		NSLog(@"Censor is ON");
	} else{
		censor = 0;
		NSLog(@"Censor is OFF");
	}
	
	NSLog(@"config before: %@", _config_arr);
	
	
	[PersistentData setConfigData: [NSString stringWithFormat:@"%d", censor] type:@"censor"];
	
	[self getConfigData];

	NSLog(@"config after: %@", _config_arr);

}


- (IBAction)sliderValueChanged:(id)sender
{
	[PersistentData setConfigData: [NSNumber numberWithFloat:_slider.value] type:@"rate"];

	[self getConfigData];
	[self updateSlider];
}

-(IBAction)openMail:(id)sender
{
	if ([MFMailComposeViewController canSendMail]) {
		MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
		
		mailer.mailComposeDelegate = self;
		mailer.navigationBar.tintColor = [UIColor whiteColor];
		[mailer setSubject:@"Feedback"];
		[mailer setToRecipients:_to_recipients];
		
		NSString *emailBody = @"";
		[mailer setMessageBody:emailBody isHTML:NO];
		
		[self presentViewController:mailer animated:YES completion:nil];
		
	} else {
		
		UIAlertController *alert = [UIAlertController
								 alertControllerWithTitle:@"Failure"
								 message:@"Your device doesn't support the composer sheet"
								 preferredStyle:UIAlertControllerStyleAlert];
		
		[self presentViewController:alert animated:YES completion:nil];
		
		
		UIAlertAction *cancelAction = [UIAlertAction
									   actionWithTitle:NSLocalizedString(@"OK", @"Cancel action")
									   style:UIAlertActionStyleCancel
									   handler:^(UIAlertAction *action)
									   {
										   NSLog(@"Cancel action");
									   }];
		
		[alert addAction:cancelAction];
	}
}


-(IBAction)thanksDrew:(id)sender
{
	[self openExternalLink:@"https://soundcloud.com/yourfellowman"];
}


-(IBAction)dmWebsite:(id)sender
{
	[self openExternalLink:@"http://www.defaultmethod.com"];
}


-(IBAction)linkAppStoreFull:(id)sender
{
	[self openExternalLink:@"https://itunes.apple.com/us/app/expletive/id1024987499?ls=1&mt=8"];
}


-(IBAction)linkAppStoreLite:(id)sender
{
	[self openExternalLink:@"https://itunes.apple.com/us/app/expletive-lite/id1024990700?mt=8"];
}


-(IBAction)linkTwitter:(id)sender
{
	[self openExternalLink:@"https://twitter.com/default_method"];
}


-(IBAction)linkFacebook:(id)sender
{
	[self openExternalLink:@"https://www.facebook.com/defaultmethod/"];
}


- (IBAction)linkPrivacy:(id)sender
{
	[self openExternalLink:@"http://www.defaultmethod.com/privacy.html"];
}


-(void)openExternalLink:(NSString *)externalLink
{
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString: externalLink] options:@{} completionHandler:nil];
}


-(IBAction)tellYourFriends:(id)sender
{
	NSMutableArray *sharingItems = [NSMutableArray new];
	
	NSString *share_text = @"Hey, checkout default_method and the app Expletive. It's pretty !*%@-ing sweet.\n";
	NSString *share_url = @"http://www.defaultmethod.com";
	UIImage *share_img = [UIImage imageNamed:@"dm_logo"];
	
	[sharingItems addObject: share_text];
	[sharingItems addObject: share_url];
	[sharingItems addObject: share_img];
	
	
	UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
	
	
	[self presentViewController:activityController animated:YES completion:nil];
}

@end
