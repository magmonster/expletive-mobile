//
//  SettingsViewController.m
// Expletive
//
//  Created by Drew Bombard on 11/13/15.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface SettingsViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIView *containerView;

@end


@implementation SettingsViewController


#pragma mark - App Setup
- (void)viewDidLoad
{
    [super viewDidLoad];
		
	[[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
}


#pragma mark - Button Actions
- (IBAction)dismissModal:(id)sender
{
	[self dismissViewControllerAnimated:YES completion:nil];
}





@end
