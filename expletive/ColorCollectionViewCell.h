//
//  ColorCollectionViewCell.h
//  Expletive
//
//  Created by Drew Bombard on 4/26/18.
//  Copyright © 2018 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>

// Data
#import "PersistentData.h"

@interface ColorCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) NSIndexPath *indexPath;
@property (strong, nonatomic) IBOutlet UIButton *btn_color_theme;
@property (strong, nonatomic) IBOutlet UIImageView *img_selected_color;

@end



